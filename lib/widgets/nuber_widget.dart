import 'package:flutter/material.dart';

class NumberWidget extends StatefulWidget {
  String number;
  Color color;

  NumberWidget(this.number, this.color);

  @override
  NumberWidgetState createState() => NumberWidgetState();
}

class NumberWidgetState extends State<NumberWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        elevation: 3.0,
        color: widget.color,
        child: InkWell(
          onTap: (){},
          child: Text(widget.number, textScaleFactor: 10,),
        ),
      ),
    );
  }
}
