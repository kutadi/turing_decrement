import 'dart:async';

import 'package:flutter/material.dart';

import 'package:rxdart/rxdart.dart';
import 'package:turing/widgets/nuber_widget.dart';

import 'package:turing/blocs/bloc_provider.dart';

enum TuringState { q0, q1, q2, q3 }

class TuringBloc implements BlocBase {
  var _numbers = List<NumberWidget>();
  TuringState currentState;
  int currentIndex;
  bool started = false;

  BehaviorSubject<List<NumberWidget>> _numbersController =
      BehaviorSubject<List<NumberWidget>>();
  Sink<List<NumberWidget>> get _inNumberList => _numbersController.sink;
  Stream<List<NumberWidget>> get outNumberList => _numbersController.stream;

  BehaviorSubject<bool> _startController = BehaviorSubject<bool>();
  Sink<bool> get _inStart => _startController.sink;
  Stream<bool> get outStart => _startController.stream;

  TuringBloc() {
    _initList();
  }

  void dispose() {
    _numbersController.close();
    _startController.close();
  }

  void _initList() {
    _numbers
      ..add(NumberWidget('1', Colors.grey))
      ..add(NumberWidget('0', Colors.grey))
      ..add(NumberWidget('0', Colors.grey))
      ..add(NumberWidget('1', Colors.grey));
    _inNumberList.add(_numbers);
    notifyStarted();
  }

  void start() {
    currentState = TuringState.q1;
    currentIndex = _numbers.length - 1;

    started = true;
    notifyStarted();

    _runTuringMachine();
  }

  void changeNumbers(String number) {
    var list = List<NumberWidget>();
    
    number.split('').forEach((i) => list.add(NumberWidget(i, Colors.grey))); 

    _numbers
      ..clear()
      ..addAll(list);

    _inNumberList.add(_numbers);
  }

  void _runTuringMachine() async {
    while (currentState != TuringState.q0) {
      switch (currentState) {
        case TuringState.q0:
          {}
          break;

        case TuringState.q1:
          {
            _numbers[currentIndex].color = Colors.blue;
            notify();

            await Future.delayed(
              const Duration(seconds: 1),
              () {
                if (_numbers[currentIndex].number == '0') {
                  _numbers[currentIndex].number = '9';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentIndex--;
                } else if (_numbers[currentIndex].number == '1') {
                  _numbers[currentIndex].number = '0';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentIndex--;
                  currentState = TuringState.q2;
                } else if (_numbers[currentIndex].number == '2') {
                  _numbers[currentIndex].number = '1';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '3') {
                  _numbers[currentIndex].number = '2';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '4') {
                  _numbers[currentIndex].number = '3';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '5') {
                  _numbers[currentIndex].number = '4';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '6') {
                  _numbers[currentIndex].number = '5';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '7') {
                  _numbers[currentIndex].number = '6';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '8') {
                  _numbers[currentIndex].number = '7';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '9') {
                  _numbers[currentIndex].number = '8';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                }
              },
            );
          }
          break;

        case TuringState.q2:
          {
            if (currentIndex > 0) {
              _numbers[currentIndex].color = Colors.blue;
              notify();
            }

            await Future.delayed(
              const Duration(seconds: 1),
              () {
                if (currentIndex < 0) {
                  currentIndex++;
                  currentState = TuringState.q3;
                } else if (_numbers[currentIndex].number == '0') {
                  _numbers[currentIndex].number = '0';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '1') {
                  _numbers[currentIndex].number = '1';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '2') {
                  _numbers[currentIndex].number = '2';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '3') {
                  _numbers[currentIndex].number = '3';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '4') {
                  _numbers[currentIndex].number = '4';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '5') {
                  _numbers[currentIndex].number = '5';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '6') {
                  _numbers[currentIndex].number = '6';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '7') {
                  _numbers[currentIndex].number = '7';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '8') {
                  _numbers[currentIndex].number = '8';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                } else if (_numbers[currentIndex].number == '9') {
                  _numbers[currentIndex].number = '9';
                  _numbers[currentIndex].color = Colors.grey;
                  notify();
                  currentState = TuringState.q0;
                }
              },
            );
          }
          break;

        case TuringState.q3:
          {
            _numbers[currentIndex].color = Colors.blue;
            notify();

            await Future.delayed(
              const Duration(seconds: 1),
              () {
                if (_numbers[currentIndex].number == '0') {
                  _numbers.removeAt(currentIndex);
                  currentState = TuringState.q0;
                }
              },
            );
          }
          break;
      }
      notify();
    }
    started = false;
    notifyStarted();
  }

  void notify() {
    var list = List<NumberWidget>();
    _numbers.forEach(
        (number) => list.add(NumberWidget(number.number, number.color)));
    _inNumberList.add(list);
  }

  void notifyStarted() {
    _inStart.add(started);
  }
}
