import 'package:flutter/material.dart';

import 'package:turing/blocs/bloc_provider.dart';
import 'package:turing/blocs/turing_bloc.dart';
import 'package:turing/pages/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Turing Machine',
      home: BlocProvider(
        bloc: TuringBloc(),
        child: HomePage(),
      ),
    );
  }
}
