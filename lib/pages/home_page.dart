import 'package:flutter/material.dart';

import 'package:turing/blocs/bloc_provider.dart';
import 'package:turing/widgets/nuber_widget.dart';
import 'package:turing/blocs/turing_bloc.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  var textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var turingBloc = BlocProvider.of<TuringBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Turing Machine'),
      ),
      body: StreamBuilder<List<NumberWidget>>(
        stream: turingBloc.outNumberList,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }

          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: snapshot.data,
              ),
              StreamBuilder<bool>(
                initialData: false,
                stream: turingBloc.outStart,
                builder: (context, snapshot) {
                  if (!snapshot.data) {
                    return RaisedButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              content: TextField(
                                decoration:
                                    InputDecoration(hintText: 'New number...'),
                                controller: textController,
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text('Cancel'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: Text('Save'),
                                  onPressed: () {
                                    turingBloc
                                        .changeNumbers(textController.text);
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                      child: Icon(Icons.edit),
                      color: Theme.of(context).primaryColor,
                    );
                  } else {
                    return RaisedButton(
                      onPressed: () {},
                      child: Icon(Icons.edit),
                      color: Colors.red,
                    );
                  }
                },
              ),
            ],
          );
        },
      ),
      floatingActionButton: StreamBuilder<bool>(
        stream: turingBloc.outStart,
        initialData: false,
        builder: (context, snapshot) {
          if (!snapshot.data) {
            return FloatingActionButton(
              onPressed: turingBloc.start,
              child: Icon(Icons.play_arrow),
            );
          } else {
            return FloatingActionButton(
              onPressed: () {},
              child: Icon(Icons.stop),
              backgroundColor: Colors.red,
            );
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
